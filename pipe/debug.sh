#!/usr/bin/env bash

# debug.sh
# ########
#
# Enable/Disable 'debug' mode

DEBUG_ARGS=''

if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling 'debug' mode."
  set -x
  DEBUG_ARGS='--verbose'
else
  info "Disabling 'debug' mode."
fi
