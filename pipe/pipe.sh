#!/usr/bin/env bash
#
# Deploy update NPM(s) packages
#
# Required globals :
#   PR_USER
#   PR_PASS
#   BRANCH_MAIN
#   CMD_BUILD_DEV
#   CMD_BUILD_PROD
#
# Optional globasl :
#   PACKAGECLOUD_TOKEN
#   PACKAGECLOUD_REPO
#   DEBUG

source "$(dirname "$0")/common.sh"

# required parameters
PR_USER=${PR_USER:?'PR_USER variable missing.'}
PR_PASS=${PR_PASS:?'PR_PASS variable missing.'}
BRANCH_MAIN=${BRANCH_MAIN:?'BRANCH_MAIN variable missing.'}
CMD_BUILD_DEV=${CMD_BUILD_DEV:?'CMD_BUILD_DEV variable missing.'}
CMD_BUILD_PROD=${CMD_BUILD_PROD:?'CMD_BUILD_PROD variable missing.'}

# optional parameters
DEBUG=${DEBUG:="false"}
PACKAGECLOUD_TOKEN=${PACKAGECLOUD_TOKEN:=""}
PACKAGECLOUD_REPO=${PACKAGECLOUD_REPO:=""}

info "Enable/Disable 'debug' mode"
# source $(pwd)/pipe/debug.sh
DEBUG_ARGS=''

if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling 'debug' mode."
  set -x
  DEBUG_ARGS='--verbose'
else
  info "Disabling 'debug' mode."
fi

info "Starting pipe execution ..."
# source $(pwd)/pipe/package_cloud.sh
if [[ -z "${PACKAGECLOUD_TOKEN}" ]] && [[ -z "${PACKAGECLOUD_REPO}" ]]; then
  info 'Use this Pipe without PackageCloud.io service'
else
  info 'Use this Pipe with PackageCloud.io service'

  echo "
always-auth=true
registry=https://packagecloud.io/${PACKAGECLOUD_REPO}/npm/
//packagecloud.io/${PACKAGECLOUD_REPO}/npm/:_authToken=${PACKAGECLOUD_TOKEN}" > ~/.npmrc
fi

info "Choose JS package manager"
# source $(pwd)/pipe/package_manager.sh
if [[ -f "yarn.lock" ]]; then
  info "File 'yarn.lock' exist so use 'yarn' software for testing update package(s)."
  PACKAGE_MANAGER='yarn'
else
  info "File 'yarn.lock' doesn't exist so use 'npm' software for testing update package(s)."
  PACKAGE_MANAGER='npm'
fi

if [[ "${DEBUG}" == "true" ]]; then
  info "Package Manager ${PACKAGE_MANAGER} version :"
  ${PACKAGE_MANAGER} --version

  info "Node Version :"
  node --version
fi

info 'Install actual dependency'
# source $(pwd)/pipe/install.sh
${PACKAGE_MANAGER} install

info "Update branch \"${BRANCH_MAIN}\""

if [ ! -z "${GIT_USER}" ] && [ ! -z "${GIT_PASS}" ]; then
  git remote set-url origin "https://${GIT_USER}:${GIT_PASS}@bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}"
fi

# See https://community.atlassian.com/t5/Bitbucket-questions/Bitbucket-Pipe-push-back-to-repo/qaq-p/1096417
git config http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy http://host.docker.internal:29418/
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch origin develop
git pull origin develop

info 'Install update packages if necessary'
${PACKAGE_MANAGER} update

git status
git rebase --autostash origin/develop

info 'Create Pull Request if packages have an update'
# source $(pwd)/pipe/pull_request.sh
if [ -n "$(git diff-index --name-only HEAD --)" ]; then
  info "Check build version development"
  ${PACKAGE_MANAGER} run ${CMD_BUILD_DEV}

  info "Check build version production"
  ${PACKAGE_MANAGER} run ${CMD_BUILD_PROD}

  info "Update packages has successfully executed :"
  apt update
  pat install -y python-pip
  pip install semversioner

  info "-> Create new release number"
  semversioner add-change --type patch 'Update JS packages'
  git add .change

  info '-> Update Repository'
  git commit -am 'Auto update JS packages [skip ci][skip build]'
  git push origin ${BITBUCKET_BRANCH}

  info "-> Create Pull Request for repository : ${BITBUCKET_REPO_FULL_NAME}"
  curl https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_FULL_NAME}/pullrequests \
    -u $PR_USER:$PR_PASS \
    --request POST \
    --header 'Content-Type: application/json' \
    --data "{
        \"title\": \"Update package(s) NPM\",
        \"source\": { \"branch\": { \"name\": \"${BITBUCKET_BRANCH}\" } },
        \"destination\": { \"branch\": { \"name\": \"${BRANCH_MAIN}\" } }
    }"

  if [[ "${status}" -eq 0 ]]; then
    success "Repository has successfully updated !"

    exit 0
  else
    fail "Failed to update repository !"

    exit 1
  fi
fi

if [[ "${status}" -eq 0 ]]; then
  success "Repository has not updated. No package update necessary."

  exit 0
else
  fail "Failed to update repository !"

  exit 1
fi
