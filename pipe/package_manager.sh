#!/usr/bin/env bash

# package_manager.sh
# ##################
#
# Choose between NPM or YARN for manager JS package

if [[ -f "yarn.lock" ]]; then
  info "File 'yarn.lock' exist so use 'yarn' software for testing update package(s)."
  PACKAGE_MANAGER='yarn'
else
  info "File 'yarn.lock' doesn't exist so use 'npm' software for testing update package(s)."
  PACKAGE_MANAGER='npm'
fi

if [[ "${DEBUG}" == "true" ]]; then
  info "Package Manager ${PACKAGE_MANAGER} version :"
  ${PACKAGE_MANAGER} --version

  info "Node Version :"
  node --version
fi
