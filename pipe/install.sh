#!/usr/bin/env bash

# install.sh
# ##########
#
# Install packages JS packages,
# update repository and finally update packages.

${PACKAGE_MANAGER} install

info "Update branch \"${BRANCH_MAIN}\""
# See https://community.atlassian.com/t5/Bitbucket-questions/Bitbucket-Pipe-push-back-to-repo/qaq-p/1096417
git config http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy http://host.docker.internal:29418/
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch origin develop
git pull origin develop

info 'Install update packages if necessary'
${PACKAGE_MANAGER} update

git status
git rebase --autostash origin/develop
