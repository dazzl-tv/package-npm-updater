#!/usr/bin/env bash

# package_cloud.sh
# ################
#
# Verify if PACKAGE_CLOUD option is enabled.
# if true so write a file '.~/.npmrc' with authorization
# to read package and download.

if [[ -z "${PACKAGECLOUD_TOKEN}" ]] && [[ -z "${PACKAGECLOUD_REPO}" ]]; then
  info 'Use this Pipe without PackageCloud.io service'
else
  info 'Use this Pipe with PackageCloud.io service'

  echo "
always-auth=true
registry=https://packagecloud.io/${PACKAGECLOUD_REPO}/npm/
//packagecloud.io/${PACKAGECLOUD_REPO}/npm/:_authToken=${PACKAGECLOUD_TOKEN}" > ~/.npmrc
fi
