#!/usr/bin/env bash

# pull_request.sh
# ###############
#
# If repository has been changed with rebase command
# upgrade create Pull Request to repository,
# if build for development & production has correctly executed.

if [ -n "$(git diff-index --name-only HEAD --)" ]; then
  info "Check build version development"
  ${PACKAGE_MANAGER} run ${CMD_BUILD_DEV}

  info "Check build version production"
  ${PACKAGE_MANAGER} run ${CMD_BUILD_PROD}

  info "Update packages has successfully executed :"
  apt update
  pat install -y python-pip
  pip install semversioner

  info "-> Create new release number"
  semversioner add-change --type patch 'Update JS packages'
  git add .change

  info '-> Update Repository'
  git commit -am 'Auto update JS packages [skip ci][skip build]'
  git push origin ${BITBUCKET_BRANCH}

  info "-> Create Pull Request for repository : ${BITBUCKET_REPO_FULL_NAME}"
  curl https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_FULL_NAME}/pullrequests \
    -u $PR_USER:$PR_PASS \
    --request POST \
    --header 'Content-Type: application/json' \
    --data "{
        \"title\": \"Update package(s) NPM\",
        \"source\": { \"branch\": { \"name\": \"${BITBUCKET_BRANCH}\" } },
        \"destination\": { \"branch\": { \"name\": \"${BRANCH_MAIN}\" } }
    }"

  if [[ "${status}" -eq 0 ]]; then
    success "Repository has successfully updated !"

    exit 0
  else
    fail "Failed to update repository !"

    exit 1
  fi
fi
