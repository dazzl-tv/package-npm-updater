#!/usr/bin/env bats
#
# Test NPM Package(s) Updater
#

set -e

PR_USER="${PR_USER}"
PR_PASS="${PR_PASS}"
BRANCH_MAIN="${BRANCH_MAIN}"
CMD_BUILD_DEV="${CMD_BUILD_DEV}"
CMD_BUILD_PROD="${CMD_BUILD_PROD}"
DEBUG='false'

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/package-npm-updater"}

  rm -f package.json
  cp test/npm.json package.json
  run docker build -t ${DOCKER_IMAGE} .
}

@test 'Missing "PR_USER" variables environments' {
  run docker run \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "PR_USER variable missing." ]]
}

@test 'Missing "PR_PASS" variables environments' {
  run docker run \
    -e PR_USER="${PR_USER}" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "PR_PASS variable missing." ]]
}

@test 'Missing "BRANCH_MAIN" variables environments' {
  run docker run \
    -e PR_USER="${PR_USER}" \
    -e PR_PASS="${PR_PASS}" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "BRANCH_MAIN variable missing." ]]
}

@test 'Missing "CMD_BUILD_DEV" variable environements' {
  run docker run \
    -e PR_USER="${PR_USER}" \
    -e PR_PASS="${PR_PASS}" \
    -e BRANCH_MAIN="${BRANCH_MAIN}" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "CMD_BUILD_DEV variable missing." ]]
}

@test 'Missing "CMD_BUILD_PROD" variable environments' {
  run docker run \
    -e PR_USER="${PR_USER}" \
    -e PR_PASS="${PR_PASS}" \
    -e BRANCH_MAIN="${BRANCH_MAIN}" \
    -e CMD_BUILD_DEV="${CMD_BUILD_DEV}" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "CMD_BUILD_PROD variable missing." ]]
}

@test "Success update" {
  run docker run \
    -e PR_USER="${PR_USER}" \
    -e PR_PASS="${PR_PASS}" \
    -e BRANCH_MAIN="${BRANCH_MAIN}" \
    -e CMD_BUILD_DEV="${CMD_BUILD_DEV}" \
    -e CMD_BUILD_PROD="${CMD_BUILD_PROD}" \
    -e PACKAGECLOUD_REPO="" \
    -e PACKAGECLOUD_TOKEN="" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  # [[ "${status}" == "0" ]]
  # [[ "${output}" =~ "Repository has successfully updated !" ]]
  [[ "${status}" == "128" ]]
  [[ "${output}" =~ "Failed to connect to localhost port 29418: Connection refused" ]]
}
