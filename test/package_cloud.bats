#!/usr/bin/env bats
#
# Test when have PackageCloud configured
#

set -e

PR_USER="${PR_USER}"
PR_PASS="${PR_PASS}"
BRANCH_MAIN="${BRANCH_MAIN}"
CMD_BUILD_DEV="${CMD_BUILD_DEV}"
CMD_BUILD_PROD="${CMD_BUILD_PROD}"
PACKAGECLOUD_REPO="${PACKAGECLOUD_REPO}"
PACKAGECLOUD_TOKEN="${PACKAGECLOUD_TOKEN}"
DEBUG='false'

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/package-npm-updater"}

  rm -f package.json
  cp test/package_cloud.json package.json
  run docker build -t ${DOCKER_IMAGE} .
}

@test "Success update" {
  run docker run \
    -e PR_USER="${PR_USER}" \
    -e PR_PASS="${PR_PASS}" \
    -e BRANCH_MAIN="${BRANCH_MAIN}" \
    -e CMD_BUILD_DEV="${CMD_BUILD_DEV}" \
    -e CMD_BUILD_PROD="${CMD_BUILD_PROD}" \
    -e PACKAGECLOUD_REPO="${PACKAGECLOUD_REPO}" \
    -e PACKAGECLOUD_TOKEN="${PACKAGECLOUD_TOKEN}" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "128" ]]
  [[ "${output}" =~ "Failed to connect to localhost port 29418: Connection refused" ]]
}
