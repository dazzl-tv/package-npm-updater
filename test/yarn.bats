#!/usr/bin/env bats
#
# Test YARN Package(s) Updater
#

set -e

PR_USER="${PR_USER}"
PR_PASS="${PR_PASS}"
BRANCH_MAIN="${BRANCH_MAIN}"
CMD_BUILD_DEV="${CMD_BUILD_DEV}"
CMD_BUILD_PROD="${CMD_BUILD_PROD}"
DEBUG='false'

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/package-npm-updater"}

  rm -f package.json
  cp test/yarn.json package.json
  yarn install
  run docker build -t ${DOCKER_IMAGE} .
}

@test "Success update" {
  run docker run \
    -e PR_USER="${PR_USER}" \
    -e PR_PASS="${PR_PASS}" \
    -e BRANCH_MAIN="${BRANCH_MAIN}" \
    -e CMD_BUILD_DEV="${CMD_BUILD_DEV}" \
    -e CMD_BUILD_PROD="${CMD_BUILD_PROD}" \
    -e PACKAGECLOUD_REPO="" \
    -e PACKAGECLOUD_TOKEN="" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}

  # [[ "${status}" == "0" ]]
  # [[ "${output}" =~ "Repository has successfully updated !" ]]
  [[ "${status}" == "128" ]]
  [[ "${output}" =~ "Failed to connect to localhost port 29418: Connection refused" ]]
}
