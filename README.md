# Bitbucket Pipelines Pipe: Package Cloud Publish

Update the package(s) NPM to repositry with Pull Request.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: dazzl-tv/package-npm-updater:2.0.2
  variables:
    PR_USER: '<string>'
    PR_PASS: '<string>'
    PH_USER: '<string>'
    PH_PASS: '<string>'
    BRANCH_MAIN: '<string>'
    CMD_BUILD_DEV: '<string>'
    CMD_BUILD_PROD: '<string>'
    # PACKAGECLOUD_TOKEN: '<string>' # Optional.
    # PACKAGECLOUD_REPO: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                 | Usage                                                                 |
| ------------------------ | ----------------------------------------------------------            |
| PR_USER (\*)             | Username used for Pull Request                                        |
| PR_PASS (\*)             | Password (app password) used for Pull Request                         |
| GIT_USER                 | Username git if you have Branch Permission configured                 |
| GIT_PASS                 | Password (app password) if you have Branch Permission configured      |
| BRANCH_MAIN (\*)         | Branch destination to pull request                                    |
| CMD_BUILD_DEV (\*)       | Command executed with npm for building (development) your application |
| CMD_BUILD_PROD (\*)      | Command executed with npm for building (production) your application  |
| PACKAGECLOUD_TOKEN       | API Token. Permitted to publish a package in Package Cloud            |
| PACKAGECLOUD_REPO        | Name to repository                                                    |
| DEBUG                    | Turn on extra debug information. Default: `false`.                    |

_(\*) = required variable._

## Details

Rebase your current branch with $BRANCH_MAIN and
update NPM dependencies if rebase has modify your repository.

When project has been updated try to build your application
in development and production environment.

If successfully create a Pull Request for $BRANCH_MAIN.

## Examples

Simple example :

```
script:
- pipe: dazzl-tv/package-npm-updater:2.0.2
    variables:
    PR_USER: 'my_username'
    PR_PASS: 'my_app_password'
    BRANCH_MAIN: 'master'
    CMD_BUILD_DEV: 'build:dev'
    CMD_BUILD_PROD: 'build'
```

With PackageCloud.io service :
```
script:
- pipe: dazzl-tv/package-npm-updater:2.0.2
    variables:
    PR_USER: 'my_username'
    PR_PASS: 'my_app_password'
    BRANCH_MAIN: 'develop'
    CMD_BUILD_DEV: 'build:dev'
    CMD_BUILD_PROD: 'build'
    PACKAGECLOUD_TOKEN: 'ldfodr5sd65b1st65b1' # Optional.
    PACKAGECLOUD_REPO: 'my-repo/javascript' # Optional.
```

## TODO

Nothing :D

## Support

If you’d like help with this pipe, or you have an issue or feature request,
let us know. The pipe is maintained by developer@adazzl.tv.

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License

Copyright (c) 2018 DazzlTV and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
