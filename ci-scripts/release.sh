#!/usr/bin/env bash

set -ex
IMAGE=$1

##
# Step 1: Generate new version
##
previous_version=$(semversioner current-version)
semversioner release
new_version=$(semversioner current-version)

##
# Step 2: Generate CHANGELOG.md
##
echo "Generating CHANGELOG.md file..."
semversioner changelog > CHANGELOG.md
# Use new version in the README.md examples
echo "Replace version '$previous_version' to '$new_version' in README.md ..."
sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" README.md
# Use new version in the pipe.yml metadata file
echo "Replace version '$previous_version' to '$new_version' in pipe.yml ..."
sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" pipe.yml

##
# Step 3: Build and push docker image
##
echo "Build and push docker image..."
echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
docker build -t ${IMAGE} .
docker tag ${IMAGE} ${IMAGE}:${new_version}
docker push ${IMAGE}

##
# Step 4: Configure GIt
##
if [ -z "${BPL_GIT_USER}" ] || [ -z "${BPL_GIT_PASS}" ]; then
  echo "Missing variable environment \"\$BPL_GIT_USER\" || \"\$BPL_GIT_PASS\" !"

  exit 1
fi
git remote set-url origin "https://${BPL_GIT_USER}:${BPL_GIT_PASS}@bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}"

##
# Step 5: Commit back to the repository
##
echo "Committing updated files to the repository..."
git add .
git commit -m "Update files for new version '${new_version}' [skip ci]"
git push origin ${BITBUCKET_BRANCH}

##
# Step 6: Tag the repository
##
echo "Tagging for release ${new_version}" "${new_version}"
git tag -a -m "Tagging for release ${new_version}" "${new_version}"
git push origin ${new_version}
