#!/usr/bin/env bash

set -ex

##
# Step 1: Install OS dependencies
##
apt update
apt install -y apt-transport-https ca-certificates jq

##
# Step 2: Install YARN program
##
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

apt update
apt install -y yarn

##
# Step 3: Install BATS program
##
npm install -g bats
