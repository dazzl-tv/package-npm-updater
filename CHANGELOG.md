# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.0.2

- patch: Fix test if GIT_USER/PASS exist

## 2.0.1

- patch: Add var for git & Branch Permission

## 2.0.0

- major: First release
- minor: Add yarn package manager
- minor: Create release with pull request
- patch: Add bitbucket host
- patch: Add exit code
- patch: Add git proxy
- patch: Add host in build image
- patch: Apply debug mode
- patch: Change software sendind request
- patch: Fix URL path for curl command
- patch: Fix argument add host
- patch: Fix dependecies for container
- patch: Fix push master branch
- patch: Precise node version
- patch: Test fix import other scripts
- patch: Test in real
- patch: Use curl instead httpie
- patch: Use verbose mode

## 1.2.0

- minor: Create release with pull request

## 1.1.0

- minor: Add yarn package manager

## 1.0.11

- patch: Fix URL path for curl command

## 1.0.10

- patch: Use verbose mode

## 1.0.9

- patch: Add exit code

## 1.0.8

- patch: Use curl instead httpie

## 1.0.7

- patch: Change software sendind request

## 1.0.6

- patch: Add git proxy
- patch: Test in real

## 1.0.5

- patch: Apply debug mode

## 1.0.4

- patch: Add host in build image
- patch: Fix argument add host

## 1.0.3

- patch: Add bitbucket host

## 1.0.2

- patch: Precise node version

## 1.0.1

- patch: Fix dependecies for container

## 1.0.0

- major: First release
