FROM node:11-alpine

LABEL author="jeremy.vaillant@dazzl.tv"
LABEL description="Docker image for execute pipe."

RUN apk add --update --no-cache \
    build-base python git bash curl openssh \
  && wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe /

ENTRYPOINT ["/pipe.sh"]
